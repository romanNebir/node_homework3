const { Load } = require("../models/loadModel");
const { Truck } = require("../models/truckModel");

module.exports.getLoads = async (req, res) => {
  const limit = parseInt(req.query.limit) || 10;
  const offset = parseInt(req.query.offset) || 0;
  let loads = [];
  if (req.user.role === "DRIVER") {
    console.log("driver");
    loads = await Load.find({
      assigned_to: req.user._id,
      status: req.query.status || /\w+/,
    })
      .skip(offset)
      .limit(limit);
  } else if (req.user.role === "SHIPPER") {
    console.log("shipper", req.query.status);
    loads = await Load.find({
      created_by: req.user._id,
      status: req.query.status || /\w+/,
    })
      .skip(offset)
      .limit(limit);
  }
  res.json({ loads });
};

module.exports.addLoad = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Only SHIPPER can create loads" });
  }
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  const load = new Load({
    created_by: req.user._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });
  await load.save();
  res.json({ message: "Load created successfully" });
};

module.exports.getActiveLoad = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const load = await Load.findOne({
    assigned_to: req.user._id,
    status: "ASSIGNED",
  });
  if (!load) {
    return res.status(400).json({ message: "No active loads" });
  }
  res.json({ load });
};

module.exports.nextLoadState = async (req, res) => {
  if (req.user.role !== "DRIVER") {
    return res.status(400).json({ message: "Available only for driver" });
  }
  const load = await Load.findOne({
    assigned_to: req.user._id,
    status: "ASSIGNED",
  });
  if (!load) {
    return res.status(400).json({ message: "No active loads" });
  }
  const states = [
    "En route to Pick Up",
    "Arrived to Pick Up",
    "En route to delivery",
    "Arrived to delivery",
  ];
  const index = states.indexOf(load.state);
  if (index < states.length - 1) {
    load.state = states[index + 1];
    load.logs.push({
      message: `Load state changed to ${load.state}`,
      time: Date.now(),
    });
    if (load.state === "Arrived to delivery") {
      load.status = "SHIPPED";
      load.logs.push({
        message: `Load status changed to ${load.status}`,
        time: Date.now(),
      });
      await Truck.findOneAndUpdate(
        { assigned_to: load.assigned_to },
        { status: "IS" }
      );
    }
    await load.save();
    res.json({ message: `Load state changed to ${load.state}` });
  }
  res.status(400).json({ message: "Load state can no longer be changed" });
};

module.exports.getLoad = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Available only for SHIPPER" });
  }
  const load = await Load.findOne({
    created_by: req.user._id,
    _id: req.params.id,
  });
  if (!load) {
    res.status(400).json({ message: `No load with id ${req.params.id}` });
  }
  res.json({ load });
};

module.exports.updateLoad = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Available only for SHIPPER" });
  }
  const load = await Load.findById(req.params.id);
  if (!load || load.created_by !== req.user._id) {
    return res.status(400).json({ message: "You have no load with such id" });
  }
  if (load.status !== "NEW") {
    return res.status(400).json({ message: "You can not update this load" });
  }
  await Load.findByIdAndUpdate(req.params.id, { ...req.body });
  res.json({ message: "Load details changed successfully" });
};

module.exports.deleteLoad = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Available only for SHIPPER" });
  }
  const load = await Load.findById(req.params.id);
  if (!load || load.created_by !== req.user._id) {
    return res.status(400).json({ message: "You have no load with such id" });
  }
  if (load.status !== "NEW") {
    return res.status(400).json({ message: "You can not delete this load" });
  }
  await Load.findByIdAndDelete(req.params.id);
  res.json({ message: "Load deleted successfully" });
};

module.exports.postLoad = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Available only for SHIPPER" });
  }
  if ((await Load.findById(req.params.id)).status !== "NEW") {
    return res.status(400).json({ message: "You can`t post this load" });
  }
  await Load.findByIdAndUpdate(req.params.id, { status: "POSTED" });
  const load = await Load.findById(req.params.id);

  const truck = await Truck.findOne({
    assigned_to: { $ne: "unassigned" },
    status: "IS",
    "dimensions.width": { $gt: load.dimensions.width },
    "dimensions.length": { $gt: load.dimensions.length },
    "dimensions.height": { $gt: load.dimensions.height },
    payload: { $gt: load.payload },
  });

  if (!truck) {
    await load.updateOne({
      status: "NEW",
      $push: { logs: { message: "Truck now found", time: Date.now() } },
    });
    return res
      .status(400)
      .json({ message: "Load posted with fail", driver_found: false });
  } else {
    await truck.updateOne({ status: "OL" });
    await load.updateOne({
      status: "ASSIGNED",
      state: "En route to Pick Up",
      assigned_to: truck.assigned_to,
      $push: {
        logs: {
          message: `Load assigned to driver with id ${truck.assigned_to}`,
          time: Date.now(),
        },
      },
    });
    res.json({ message: "Load posted successfully", driver_found: true });
  }
};

module.exports.getShippingInfo = async (req, res) => {
  if (req.user.role !== "SHIPPER") {
    return res.status(400).json({ message: "Available only for SHIPPER" });
  }
  const load = await Load.findById(req.params.id);
  if (load.status !== "ASSIGNED") {
    return res.status(400).json({ message: "It is not an active load" });
  }
  const truck = await Truck.findOne({ assigned_to: load.assigned_to });
  res.json({ load, truck });
};
