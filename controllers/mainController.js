module.exports.getMainPage = (req, res) => {
    res.render('home', {
        title: 'Main page',
        authorized: false
    });
};

module.exports.getRegistrationPage = (req, res) => {
    res.render('registration', {
        title: 'Registration page',
        authorized: false
    });
}