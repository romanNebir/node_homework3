const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { User } = require("../models/userModel");

module.exports.register = async (req, res) => {
  const { email, password, role } = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.json({ message: "User created successfully!" });
};

module.exports.login = async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    return res
      .status(400)
      .json({ message: `No user with email '${email}' found!` });
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({ message: `Wrong password!` });
  }

  const jwtToken = jwt.sign(
    { email: user.email, _id: user._id, role: user.role },
    process.env.JWT_SECRET
  );
  res.json({ message: "Success", jwt_token: jwtToken });
};

module.exports.forgotPassword = async (req, res) => {
  const exist = await User.exists({ email: req.body.email });
  if (!exist) {
    return res.status(400).json({message: 'No user with such email'});
  }
  res.json({ message: "New password sent to your email address" });
};
