const mongoose = require("mongoose");

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: "unessigned",
  },
  status: {
    //   [ NEW, POSTED, ASSIGNED, SHIPPED ]
    type: String,
    required: true,
    default: "NEW",
  },
  state: {
    // [ En route to Pick Up, Arrived to Pick Up, En route to delivery, Arrived to delivery ]
    type: String,
    // required: true,
    default: "",
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {
      width: Number,
      length: Number,
      height: Number,
    },
    required: true,
  },
  logs: {
    type: [
      {
        message: String,
        time: Date,
      },
    ],
    // required: true
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model("Load", loadSchema);
