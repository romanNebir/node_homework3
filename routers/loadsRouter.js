const express = require("express");
const {
  getLoads,
  addLoad,
  getActiveLoad,
  nextLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo,
} = require("../controllers/loadsController");
const { asyncWrapper } = require("./helpers");
const { authMiddleware } = require("./middlewares/authMiddleware");

const router = express.Router();

router.use('*', authMiddleware)
router.get("/", asyncWrapper(getLoads));
router.post("/", asyncWrapper(addLoad));
router.get("/active", asyncWrapper(getActiveLoad));
router.patch("/active/state", asyncWrapper(nextLoadState));
router.get("/:id", asyncWrapper(getLoad));
router.put("/:id", asyncWrapper(updateLoad));
router.delete("/:id", asyncWrapper(deleteLoad));
router.post("/:id/post", asyncWrapper(postLoad));
router.get("/:id/shipping_info", asyncWrapper(getShippingInfo));

module.exports = router;
