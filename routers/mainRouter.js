const express = require("express");
const {
  getMainPage,
  getRegistrationPage,
} = require("../controllers/mainController");

const router = express.Router();

router.get("/", getMainPage);
router.get("/registration", getRegistrationPage);

module.exports = router;
